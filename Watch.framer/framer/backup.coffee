# This imports all the layers for "Watch" into watchLayers
watchLayers = Framer.Importer.load "imported/Watch"

# Welcome to Framer
console.log(watchLayers);

notificationAnimation = new Animation({
	    layer: watchLayers["Notification"],
	    properties: {opacity:0},
	    time: 0.2
	})

watchLayers["Notification"].on Events.Click, ->
	notificationAnimation.start()

notificationAnimation.on Events.AnimationEnd, ->
	watchLayers["Navigation"].opacity = 1;
	watchLayers["Navigation"].visible = true;

navigationAnimation = new Animation({
	    layer: watchLayers["Notification"],
	    properties: {opacity:0},
	    time: 0.2
	})

watchLayers["Navigation"].on Events.Click, ->
	navigationAnimation.start()
	
navigationAnimation.on Events.AnimationEnd, ->
	watchLayers["Arrival"].opacity = 1;
	watchLayers["Arrival"].visible = true;