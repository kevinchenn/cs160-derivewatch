window.__imported__ = window.__imported__ || {};
window.__imported__["Watch/layers.json.js"] = [
  {
    "maskFrame" : null,
    "id" : "A48A5377-FED2-46FA-9220-D9C1A73DCBCE",
    "visible" : true,
    "children" : [

    ],
    "image" : {
      "path" : "images\/Notification-A48A5377-FED2-46FA-9220-D9C1A73DCBCE.png",
      "frame" : {
        "y" : 0,
        "x" : 0,
        "width" : 333,
        "height" : 379
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 0,
      "width" : 333,
      "height" : 379
    },
    "name" : "Notification"
  },
  {
    "maskFrame" : null,
    "id" : "E2458559-4896-45C6-A6B4-0D341166D4B7",
    "visible" : false,
    "children" : [

    ],
    "image" : {
      "path" : "images\/Navigation-E2458559-4896-45C6-A6B4-0D341166D4B7.png",
      "frame" : {
        "y" : 0,
        "x" : 0,
        "width" : 333,
        "height" : 379
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 0,
      "width" : 333,
      "height" : 379
    },
    "name" : "Navigation"
  },
  {
    "maskFrame" : null,
    "id" : "FCF81472-D524-4FC5-9E29-55CFBD824E39",
    "visible" : false,
    "children" : [

    ],
    "image" : {
      "path" : "images\/Arrival-FCF81472-D524-4FC5-9E29-55CFBD824E39.png",
      "frame" : {
        "y" : 0,
        "x" : 0,
        "width" : 333,
        "height" : 379
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 0,
      "width" : 333,
      "height" : 379
    },
    "name" : "Arrival"
  }
]