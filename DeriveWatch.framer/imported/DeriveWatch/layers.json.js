window.__imported__ = window.__imported__ || {};
window.__imported__["DeriveWatch/layers.json.js"] = [
  {
    "maskFrame" : null,
    "id" : "C924AD6B-D7E5-45D3-9E3D-1C01902BE3E7",
    "visible" : true,
    "children" : [

    ],
    "image" : {
      "path" : "images\/Splash_Screen-C924AD6B-D7E5-45D3-9E3D-1C01902BE3E7.png",
      "frame" : {
        "y" : 0,
        "x" : 0,
        "width" : 640,
        "height" : 1136
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 0,
      "width" : 640,
      "height" : 1136
    },
    "name" : "Splash_Screen"
  },
  {
    "maskFrame" : null,
    "id" : "EC80854F-947F-43FF-BDBB-6C1E448AAC88",
    "visible" : false,
    "children" : [
      {
        "maskFrame" : null,
        "id" : "6E655A48-898C-4AB2-B33D-33850D323243",
        "visible" : true,
        "children" : [

        ],
        "image" : {
          "path" : "images\/settings_icon-6E655A48-898C-4AB2-B33D-33850D323243.png",
          "frame" : {
            "y" : 32,
            "x" : 14,
            "width" : 71,
            "height" : 55
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 32,
          "x" : 14,
          "width" : 71,
          "height" : 55
        },
        "name" : "settings_icon"
      },
      {
        "maskFrame" : null,
        "id" : "F9F2F60E-D57A-47C0-B112-53BAD2864232",
        "visible" : false,
        "children" : [

        ],
        "image" : {
          "path" : "images\/curious_off-F9F2F60E-D57A-47C0-B112-53BAD2864232.png",
          "frame" : {
            "y" : 1024,
            "x" : 1,
            "width" : 639,
            "height" : 112
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 1024,
          "x" : 1,
          "width" : 639,
          "height" : 112
        },
        "name" : "curious_off"
      },
      {
        "maskFrame" : null,
        "id" : "D0AE1C25-DF88-4AC7-94A5-B26FAF988E40",
        "visible" : true,
        "children" : [

        ],
        "image" : {
          "path" : "images\/curious_on-D0AE1C25-DF88-4AC7-94A5-B26FAF988E40.png",
          "frame" : {
            "y" : 1024,
            "x" : 1,
            "width" : 639,
            "height" : 112
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 1024,
          "x" : 1,
          "width" : 639,
          "height" : 112
        },
        "name" : "curious_on"
      },
      {
        "maskFrame" : null,
        "id" : "83580A92-01CD-4280-9A2E-A0DDCEAAA899",
        "visible" : true,
        "children" : [
          {
            "maskFrame" : null,
            "id" : "229182F9-D149-4BFF-8C11-BB26E163EDB3",
            "visible" : true,
            "children" : [

            ],
            "image" : {
              "path" : "images\/Group-229182F9-D149-4BFF-8C11-BB26E163EDB3.png",
              "frame" : {
                "y" : 576,
                "x" : 16,
                "width" : 69,
                "height" : 72
              }
            },
            "imageType" : "png",
            "layerFrame" : {
              "y" : 576,
              "x" : 16,
              "width" : 69,
              "height" : 72
            },
            "name" : "Group"
          }
        ],
        "image" : {
          "path" : "images\/green6-83580A92-01CD-4280-9A2E-A0DDCEAAA899.png",
          "frame" : {
            "y" : 576,
            "x" : 16,
            "width" : 69,
            "height" : 72
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 576,
          "x" : 16,
          "width" : 69,
          "height" : 72
        },
        "name" : "green6"
      },
      {
        "maskFrame" : null,
        "id" : "6912DDF6-B6FE-459B-AD9E-6EEFB92837A4",
        "visible" : true,
        "children" : [
          {
            "maskFrame" : null,
            "id" : "6A6B97F1-C458-479A-A6D7-84164916CE71",
            "visible" : true,
            "children" : [

            ],
            "image" : {
              "path" : "images\/Group-6A6B97F1-C458-479A-A6D7-84164916CE71.png",
              "frame" : {
                "y" : 132,
                "x" : 16,
                "width" : 68,
                "height" : 69
              }
            },
            "imageType" : "png",
            "layerFrame" : {
              "y" : 132,
              "x" : 16,
              "width" : 68,
              "height" : 69
            },
            "name" : "Group"
          }
        ],
        "image" : {
          "path" : "images\/painter11-6912DDF6-B6FE-459B-AD9E-6EEFB92837A4.png",
          "frame" : {
            "y" : 132,
            "x" : 16,
            "width" : 68,
            "height" : 69
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 132,
          "x" : 16,
          "width" : 68,
          "height" : 69
        },
        "name" : "painter11"
      },
      {
        "maskFrame" : null,
        "id" : "BC316572-F188-48CD-B18A-EE6159B5358D",
        "visible" : true,
        "children" : [

        ],
        "image" : {
          "path" : "images\/restaurant2-BC316572-F188-48CD-B18A-EE6159B5358D.png",
          "frame" : {
            "y" : 466,
            "x" : 15,
            "width" : 72,
            "height" : 63
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 466,
          "x" : 15,
          "width" : 72,
          "height" : 63
        },
        "name" : "restaurant2"
      },
      {
        "maskFrame" : null,
        "id" : "C7D82AC0-B9CC-4A1C-A725-5DEC540EC063",
        "visible" : true,
        "children" : [

        ],
        "image" : {
          "path" : "images\/cafe_not_selected-C7D82AC0-B9CC-4A1C-A725-5DEC540EC063.png",
          "frame" : {
            "y" : 341,
            "x" : 19,
            "width" : 594,
            "height" : 78
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 341,
          "x" : 19,
          "width" : 594,
          "height" : 78
        },
        "name" : "cafe_not_selected"
      },
      {
        "maskFrame" : null,
        "id" : "A85EC9E6-8D1C-44CF-B2B1-1D749C747A37",
        "visible" : false,
        "children" : [

        ],
        "image" : {
          "path" : "images\/cafe_selected-A85EC9E6-8D1C-44CF-B2B1-1D749C747A37.png",
          "frame" : {
            "y" : 330,
            "x" : 0,
            "width" : 640,
            "height" : 112
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 330,
          "x" : 0,
          "width" : 640,
          "height" : 112
        },
        "name" : "cafe_selected"
      }
    ],
    "image" : {
      "path" : "images\/Categories-EC80854F-947F-43FF-BDBB-6C1E448AAC88.png",
      "frame" : {
        "y" : 0,
        "x" : 0,
        "width" : 640,
        "height" : 1136
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 0,
      "width" : 640,
      "height" : 1136
    },
    "name" : "Categories"
  },
  {
    "maskFrame" : null,
    "id" : "DEDEA66D-5568-46CD-9AC3-FABD9987B7C4",
    "visible" : false,
    "children" : [
      {
        "maskFrame" : null,
        "id" : "E62ACCC5-6408-4CAE-8BFC-64F869CFACBD",
        "visible" : true,
        "children" : [

        ],
        "image" : {
          "path" : "images\/sightglass_unselected-E62ACCC5-6408-4CAE-8BFC-64F869CFACBD.png",
          "frame" : {
            "y" : 358,
            "x" : 17,
            "width" : 596,
            "height" : 59
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 358,
          "x" : 17,
          "width" : 596,
          "height" : 59
        },
        "name" : "sightglass_unselected"
      },
      {
        "maskFrame" : null,
        "id" : "73C9E9ED-6396-43A4-ACB1-ACEE270B22E3",
        "visible" : false,
        "children" : [

        ],
        "image" : {
          "path" : "images\/sightglass_selected-73C9E9ED-6396-43A4-ACB1-ACEE270B22E3.png",
          "frame" : {
            "y" : 330,
            "x" : 0,
            "width" : 640,
            "height" : 112
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 330,
          "x" : 0,
          "width" : 640,
          "height" : 112
        },
        "name" : "sightglass_selected"
      }
    ],
    "image" : {
      "path" : "images\/List_of_Cafes-DEDEA66D-5568-46CD-9AC3-FABD9987B7C4.png",
      "frame" : {
        "y" : 0,
        "x" : 0,
        "width" : 640,
        "height" : 1136
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 0,
      "width" : 640,
      "height" : 1136
    },
    "name" : "List_of_Cafes"
  },
  {
    "maskFrame" : null,
    "id" : "690F70E7-311E-41EE-ADC1-54F1492E1062",
    "visible" : false,
    "children" : [
      {
        "maskFrame" : null,
        "id" : "6214B453-BAD7-40F5-9A70-CA96281D4C1E",
        "visible" : true,
        "children" : [

        ],
        "image" : {
          "path" : "images\/set_destination-6214B453-BAD7-40F5-9A70-CA96281D4C1E.png",
          "frame" : {
            "y" : 1024,
            "x" : 1,
            "width" : 639,
            "height" : 112
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 1024,
          "x" : 1,
          "width" : 639,
          "height" : 112
        },
        "name" : "set_destination"
      }
    ],
    "image" : {
      "path" : "images\/Sightglass_Coffee-690F70E7-311E-41EE-ADC1-54F1492E1062.png",
      "frame" : {
        "y" : 0,
        "x" : 0,
        "width" : 640,
        "height" : 1136
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 0,
      "width" : 640,
      "height" : 1136
    },
    "name" : "Sightglass_Coffee"
  },
  {
    "maskFrame" : null,
    "id" : "2E6A2A85-13EB-4366-A801-F4ED4FBD50F8",
    "visible" : false,
    "children" : [
      {
        "maskFrame" : null,
        "id" : "F680465E-3149-4626-8169-A4D62D50651E",
        "visible" : true,
        "children" : [

        ],
        "image" : {
          "path" : "images\/invite_friends-F680465E-3149-4626-8169-A4D62D50651E.png",
          "frame" : {
            "y" : 1024,
            "x" : 1,
            "width" : 639,
            "height" : 112
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 1024,
          "x" : 1,
          "width" : 639,
          "height" : 112
        },
        "name" : "invite_friends"
      },
      {
        "maskFrame" : null,
        "id" : "BE42CE73-E362-4517-99D6-C6D34BE487DD",
        "visible" : true,
        "children" : [

        ],
        "image" : {
          "path" : "images\/kim_unselected-BE42CE73-E362-4517-99D6-C6D34BE487DD.png",
          "frame" : {
            "y" : 599,
            "x" : 20,
            "width" : 381,
            "height" : 33
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 599,
          "x" : 20,
          "width" : 381,
          "height" : 33
        },
        "name" : "kim_unselected"
      },
      {
        "maskFrame" : null,
        "id" : "46AEE89D-6DE0-4C7F-A1D0-8EF8757C8E9A",
        "visible" : false,
        "children" : [

        ],
        "image" : {
          "path" : "images\/kim_selected-46AEE89D-6DE0-4C7F-A1D0-8EF8757C8E9A.png",
          "frame" : {
            "y" : 563,
            "x" : 0,
            "width" : 640,
            "height" : 105
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 563,
          "x" : 0,
          "width" : 640,
          "height" : 105
        },
        "name" : "kim_selected"
      }
    ],
    "image" : {
      "path" : "images\/INVITE_FRIENDS-2E6A2A85-13EB-4366-A801-F4ED4FBD50F8.png",
      "frame" : {
        "y" : 0,
        "x" : 0,
        "width" : 640,
        "height" : 1136
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 0,
      "width" : 640,
      "height" : 1136
    },
    "name" : "INVITE_FRIENDS"
  },
  {
    "maskFrame" : null,
    "id" : "5D77546A-1E0A-4148-A0A7-3354300AEF04",
    "visible" : false,
    "children" : [
      {
        "maskFrame" : null,
        "id" : "8B6E7D25-4DE3-4AC9-994F-DBC5B86D8B3E",
        "visible" : true,
        "children" : [
          {
            "maskFrame" : null,
            "id" : "63806D94-D431-4079-B722-C6546CD93143",
            "visible" : true,
            "children" : [

            ],
            "image" : {
              "path" : "images\/handle-63806D94-D431-4079-B722-C6546CD93143.png",
              "frame" : {
                "y" : 1056,
                "x" : 542,
                "width" : 86,
                "height" : 80
              }
            },
            "imageType" : "png",
            "layerFrame" : {
              "y" : 1056,
              "x" : 542,
              "width" : 86,
              "height" : 80
            },
            "name" : "handle"
          }
        ],
        "image" : {
          "path" : "images\/Controls__Switch_On_6-8B6E7D25-4DE3-4AC9-994F-DBC5B86D8B3E.png",
          "frame" : {
            "y" : 1056,
            "x" : 514,
            "width" : 114,
            "height" : 80
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 1056,
          "x" : 514,
          "width" : 114,
          "height" : 80
        },
        "name" : "Controls__Switch_On_6"
      },
      {
        "maskFrame" : null,
        "id" : "E13AC640-90C3-4938-8001-1230DDDBE60F",
        "visible" : true,
        "children" : [
          {
            "maskFrame" : null,
            "id" : "E8AF193B-638C-491C-A789-B921DF11CACD",
            "visible" : true,
            "children" : [

            ],
            "image" : {
              "path" : "images\/handle-E8AF193B-638C-491C-A789-B921DF11CACD.png",
              "frame" : {
                "y" : 966,
                "x" : 542,
                "width" : 86,
                "height" : 86
              }
            },
            "imageType" : "png",
            "layerFrame" : {
              "y" : 966,
              "x" : 542,
              "width" : 86,
              "height" : 86
            },
            "name" : "handle"
          }
        ],
        "image" : {
          "path" : "images\/Controls__Switch_On_5-E13AC640-90C3-4938-8001-1230DDDBE60F.png",
          "frame" : {
            "y" : 966,
            "x" : 514,
            "width" : 114,
            "height" : 86
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 966,
          "x" : 514,
          "width" : 114,
          "height" : 86
        },
        "name" : "Controls__Switch_On_5"
      },
      {
        "maskFrame" : null,
        "id" : "662077E0-6C9E-4BFD-AF3D-A0584DEBA90A",
        "visible" : true,
        "children" : [
          {
            "maskFrame" : null,
            "id" : "D8F243D6-90C1-4940-9C24-EAFA3C04D2A3",
            "visible" : true,
            "children" : [

            ],
            "image" : {
              "path" : "images\/handle-D8F243D6-90C1-4940-9C24-EAFA3C04D2A3.png",
              "frame" : {
                "y" : 873,
                "x" : 542,
                "width" : 86,
                "height" : 86
              }
            },
            "imageType" : "png",
            "layerFrame" : {
              "y" : 873,
              "x" : 542,
              "width" : 86,
              "height" : 86
            },
            "name" : "handle"
          }
        ],
        "image" : {
          "path" : "images\/Controls__Switch_On_4-662077E0-6C9E-4BFD-AF3D-A0584DEBA90A.png",
          "frame" : {
            "y" : 873,
            "x" : 514,
            "width" : 114,
            "height" : 86
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 873,
          "x" : 514,
          "width" : 114,
          "height" : 86
        },
        "name" : "Controls__Switch_On_4"
      },
      {
        "maskFrame" : null,
        "id" : "E0A3E36A-845E-455C-B9D1-13505D88D6A0",
        "visible" : true,
        "children" : [
          {
            "maskFrame" : null,
            "id" : "33EC6984-5FE6-431F-BF8F-A26B1ABE0056",
            "visible" : true,
            "children" : [

            ],
            "image" : {
              "path" : "images\/handle-33EC6984-5FE6-431F-BF8F-A26B1ABE0056.png",
              "frame" : {
                "y" : 775,
                "x" : 542,
                "width" : 86,
                "height" : 86
              }
            },
            "imageType" : "png",
            "layerFrame" : {
              "y" : 775,
              "x" : 542,
              "width" : 86,
              "height" : 86
            },
            "name" : "handle"
          }
        ],
        "image" : {
          "path" : "images\/Controls__Switch_On_3-E0A3E36A-845E-455C-B9D1-13505D88D6A0.png",
          "frame" : {
            "y" : 775,
            "x" : 514,
            "width" : 114,
            "height" : 86
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 775,
          "x" : 514,
          "width" : 114,
          "height" : 86
        },
        "name" : "Controls__Switch_On_3"
      },
      {
        "maskFrame" : null,
        "id" : "58FBA5F8-5926-43F2-B69D-AC4595F0F5BF",
        "visible" : true,
        "children" : [
          {
            "maskFrame" : null,
            "id" : "2D808DBD-2D47-4F5C-8B70-887B3809E4FB",
            "visible" : true,
            "children" : [

            ],
            "image" : {
              "path" : "images\/handle-2D808DBD-2D47-4F5C-8B70-887B3809E4FB.png",
              "frame" : {
                "y" : 682,
                "x" : 542,
                "width" : 86,
                "height" : 86
              }
            },
            "imageType" : "png",
            "layerFrame" : {
              "y" : 682,
              "x" : 542,
              "width" : 86,
              "height" : 86
            },
            "name" : "handle"
          }
        ],
        "image" : {
          "path" : "images\/Controls__Switch_On_2-58FBA5F8-5926-43F2-B69D-AC4595F0F5BF.png",
          "frame" : {
            "y" : 682,
            "x" : 514,
            "width" : 114,
            "height" : 86
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 682,
          "x" : 514,
          "width" : 114,
          "height" : 86
        },
        "name" : "Controls__Switch_On_2"
      },
      {
        "maskFrame" : null,
        "id" : "004D0925-F238-4196-8710-2D48238FD305",
        "visible" : true,
        "children" : [
          {
            "maskFrame" : null,
            "id" : "9EB45E7D-F42D-4D87-B2BA-C212240E6809",
            "visible" : true,
            "children" : [

            ],
            "image" : {
              "path" : "images\/handle-9EB45E7D-F42D-4D87-B2BA-C212240E6809.png",
              "frame" : {
                "y" : 574,
                "x" : 502,
                "width" : 86,
                "height" : 86
              }
            },
            "imageType" : "png",
            "layerFrame" : {
              "y" : 574,
              "x" : 502,
              "width" : 86,
              "height" : 86
            },
            "name" : "handle"
          }
        ],
        "image" : {
          "path" : "images\/Controls__Switch_Off_6-004D0925-F238-4196-8710-2D48238FD305.png",
          "frame" : {
            "y" : 574,
            "x" : 502,
            "width" : 114,
            "height" : 86
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 574,
          "x" : 502,
          "width" : 114,
          "height" : 86
        },
        "name" : "Controls__Switch_Off_6"
      },
      {
        "maskFrame" : null,
        "id" : "7442777A-930C-4D95-94ED-6E8473927A85",
        "visible" : true,
        "children" : [
          {
            "maskFrame" : null,
            "id" : "65B17D1E-27BC-4FA1-B1B9-68468DC911E8",
            "visible" : true,
            "children" : [

            ],
            "image" : {
              "path" : "images\/handle-65B17D1E-27BC-4FA1-B1B9-68468DC911E8.png",
              "frame" : {
                "y" : 460,
                "x" : 502,
                "width" : 86,
                "height" : 86
              }
            },
            "imageType" : "png",
            "layerFrame" : {
              "y" : 460,
              "x" : 502,
              "width" : 86,
              "height" : 86
            },
            "name" : "handle"
          }
        ],
        "image" : {
          "path" : "images\/Controls__Switch_Off_5-7442777A-930C-4D95-94ED-6E8473927A85.png",
          "frame" : {
            "y" : 460,
            "x" : 502,
            "width" : 114,
            "height" : 86
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 460,
          "x" : 502,
          "width" : 114,
          "height" : 86
        },
        "name" : "Controls__Switch_Off_5"
      },
      {
        "maskFrame" : null,
        "id" : "B063EAAA-904F-4712-921C-E7CE64803FCB",
        "visible" : true,
        "children" : [
          {
            "maskFrame" : null,
            "id" : "2F9C159B-9F73-4068-AC7A-36A450729E91",
            "visible" : true,
            "children" : [

            ],
            "image" : {
              "path" : "images\/handle-2F9C159B-9F73-4068-AC7A-36A450729E91.png",
              "frame" : {
                "y" : 352,
                "x" : 502,
                "width" : 86,
                "height" : 86
              }
            },
            "imageType" : "png",
            "layerFrame" : {
              "y" : 352,
              "x" : 502,
              "width" : 86,
              "height" : 86
            },
            "name" : "handle"
          }
        ],
        "image" : {
          "path" : "images\/Controls__Switch_Off_4-B063EAAA-904F-4712-921C-E7CE64803FCB.png",
          "frame" : {
            "y" : 352,
            "x" : 502,
            "width" : 114,
            "height" : 86
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 352,
          "x" : 502,
          "width" : 114,
          "height" : 86
        },
        "name" : "Controls__Switch_Off_4"
      },
      {
        "maskFrame" : null,
        "id" : "78920C0D-D1A7-4A13-9D1B-5523DCDFF51E",
        "visible" : true,
        "children" : [
          {
            "maskFrame" : null,
            "id" : "2D38764D-210F-4A49-8ADB-4BE73AEBC2AC",
            "visible" : true,
            "children" : [

            ],
            "image" : {
              "path" : "images\/handle-2D38764D-210F-4A49-8ADB-4BE73AEBC2AC.png",
              "frame" : {
                "y" : 240,
                "x" : 502,
                "width" : 86,
                "height" : 86
              }
            },
            "imageType" : "png",
            "layerFrame" : {
              "y" : 240,
              "x" : 502,
              "width" : 86,
              "height" : 86
            },
            "name" : "handle"
          }
        ],
        "image" : {
          "path" : "images\/Controls__Switch_Off_3-78920C0D-D1A7-4A13-9D1B-5523DCDFF51E.png",
          "frame" : {
            "y" : 240,
            "x" : 502,
            "width" : 114,
            "height" : 86
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 240,
          "x" : 502,
          "width" : 114,
          "height" : 86
        },
        "name" : "Controls__Switch_Off_3"
      },
      {
        "maskFrame" : null,
        "id" : "5A74BF1B-6437-4C8D-81C9-E5D649F53E6C",
        "visible" : true,
        "children" : [
          {
            "maskFrame" : null,
            "id" : "68B0C22C-5C7B-4166-B005-0F0529D8CE6B",
            "visible" : true,
            "children" : [

            ],
            "image" : {
              "path" : "images\/handle-68B0C22C-5C7B-4166-B005-0F0529D8CE6B.png",
              "frame" : {
                "y" : 126,
                "x" : 502,
                "width" : 86,
                "height" : 86
              }
            },
            "imageType" : "png",
            "layerFrame" : {
              "y" : 126,
              "x" : 502,
              "width" : 86,
              "height" : 86
            },
            "name" : "handle"
          }
        ],
        "image" : {
          "path" : "images\/Controls__Switch_Off_2-5A74BF1B-6437-4C8D-81C9-E5D649F53E6C.png",
          "frame" : {
            "y" : 126,
            "x" : 502,
            "width" : 114,
            "height" : 86
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 126,
          "x" : 502,
          "width" : 114,
          "height" : 86
        },
        "name" : "Controls__Switch_Off_2"
      },
      {
        "maskFrame" : null,
        "id" : "DD6EA547-E9C3-40DC-8D6F-27CC7B94061B",
        "visible" : true,
        "children" : [

        ],
        "image" : {
          "path" : "images\/settings_back-DD6EA547-E9C3-40DC-8D6F-27CC7B94061B.png",
          "frame" : {
            "y" : 36,
            "x" : 18,
            "width" : 40,
            "height" : 46
          }
        },
        "imageType" : "png",
        "layerFrame" : {
          "y" : 36,
          "x" : 18,
          "width" : 40,
          "height" : 46
        },
        "name" : "settings_back"
      }
    ],
    "image" : {
      "path" : "images\/Settings-5D77546A-1E0A-4148-A0A7-3354300AEF04.png",
      "frame" : {
        "y" : 0,
        "x" : 0,
        "width" : 640,
        "height" : 1136
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 0,
      "width" : 640,
      "height" : 1136
    },
    "name" : "Settings"
  },
  {
    "maskFrame" : null,
    "id" : "317A6D6D-100B-4359-94EA-BB3B668F7690",
    "visible" : false,
    "children" : [

    ],
    "image" : {
      "path" : "images\/Select_Time-317A6D6D-100B-4359-94EA-BB3B668F7690.png",
      "frame" : {
        "y" : 0,
        "x" : 0,
        "width" : 640,
        "height" : 1136
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 0,
      "width" : 640,
      "height" : 1136
    },
    "name" : "Select_Time"
  },
  {
    "maskFrame" : null,
    "id" : "58CFD69C-E56B-4999-BEC6-17EADD9BA9EA",
    "visible" : false,
    "children" : [

    ],
    "image" : {
      "path" : "images\/Start_Screen-58CFD69C-E56B-4999-BEC6-17EADD9BA9EA.png",
      "frame" : {
        "y" : 0,
        "x" : 0,
        "width" : 640,
        "height" : 1136
      }
    },
    "imageType" : "png",
    "layerFrame" : {
      "y" : 0,
      "x" : 0,
      "width" : 640,
      "height" : 1136
    },
    "name" : "Start_Screen"
  }
]