# This imports all the layers for "DeriveWatch" into derivewatchLayers
derivewatchLayers = Framer.Importer.load "imported/DeriveWatch"

# Welcome to Framer
console.log(derivewatchLayers)
console.log(derivewatchLayers["Categories"])
derivewatchLayers["Categories"].opacity = 0;
derivewatchLayers["List_of_Cafes"].opcaity = 0;
derivewatchLayers["Sightglass_Coffee"].opacity = 0;
derivewatchLayers["Settings"].opacity = 0;
derivewatchLayers["INVITE_FRIENDS"].opacity = 0;
derivewatchLayers["Select_Time"].opacity = 0;
derivewatchLayers["Start_Screen"].opacity = 0;



# Adding Hierarchy to Layers
derivewatchLayers["cafe_selected"].superLayer = derivewatchLayers["Categories"];
derivewatchLayers["cafe_not_selected"].superLayer = derivewatchLayers["Categories"];
derivewatchLayers["settings_icon"].superLayer = derivewatchLayers["Categories"];
derivewatchLayers["curious_on"].superLayer = derivewatchLayers["Categories"];
derivewatchLayers["curious_off"].superLayer = derivewatchLayers["Categories"];

derivewatchLayers["settings_back"].superLayer = derivewatchLayers["Settings"];

derivewatchLayers["sightglass_selected"].superLayer = derivewatchLayers["List_of_Cafes"];
derivewatchLayers["sightglass_unselected"].superLayer = derivewatchLayers["List_of_Cafes"];

derivewatchLayers["kim_selected"].superLayer = derivewatchLayers["INVITE_FRIENDS"];
derivewatchLayers["kim_unselected"].superLayer = derivewatchLayers["INVITE_FRIENDS"];

derivewatchLayers["set_destination"].superLayer = derivewatchLayers["Sightglass_Coffee"];

# Categories Screen Helper Functions
categoriesInit = () ->
	
	derivewatchLayers["settings_back"].off Events.Click
	
	derivewatchLayers["settings_icon"].ignoreEvents = false
	derivewatchLayers["cafe_selected"].ignoreEvents = false
	derivewatchLayers["curious_on"].ignoreEvents = false
	derivewatchLayers["curious_off"].ignoreEvents = false
	derivewatchLayers["settings_back"].ignoreEvents = true
	derivewatchLayers["sightglass_unselected"].ignoreEvents = true
	derivewatchLayers["set_destination"].ignoreEvents = true
	derivewatchLayers["kim_selected"].ignoreEvents = true
	derivewatchLayers["kim_unselected"].ignoreEvents = true
	derivewatchLayers["invite_friends"].ignoreEvents = true	
	derivewatchLayers["Categories"].subLayers[0].visible = false
	derivewatchLayers["Categories"].subLayers[1].visible = true
	
	# Settings Icon Functionality
	settingsIconAnimation = new Animation({
	    layer: derivewatchLayers["Categories"],
	    properties: {opacity:0},
	    time: 0.2
	})
	
	derivewatchLayers["settings_icon"].on Events.Click, ->
		settingsIconAnimation.start()
		
	settingsIconAnimation.on Events.AnimationEnd, -> 
		derivewatchLayers["Settings"].opacity = 1
		derivewatchLayers["Settings"].visible = true
		settingsInit()
	
	# When Cafe is Selected
	cafeSelectedAnimation = new Animation({
	    layer: derivewatchLayers["Categories"],
	    properties: {opacity:0},
	    time: 0.2
	})
	
	derivewatchLayers["cafe_not_selected"].on Events.Click, -> 
		derivewatchLayers["Categories"].subLayers[0].visible = true
		derivewatchLayers["Categories"].subLayers[1].visible = false
		cafeSelectedAnimation.start()
	
	cafeSelectedAnimation.on Events.AnimationEnd, -> 
		derivewatchLayers["List_of_Cafes"].opacity = 1
		derivewatchLayers["List_of_Cafes"].visible = true
		listOfCafesInit()
		
	# Toggle Curious
	derivewatchLayers["curious_on"].on Events.Click, ->
		derivewatchLayers["Categories"].subLayers[6].visible = true
		derivewatchLayers["Categories"].subLayers[5].visible = false
		derivewatchLayers["curious_on"].ignoreEvents = true
		derivewatchLayers["curious_off"].ignoreEvents = false
	
	derivewatchLayers["curious_off"].on Events.Click, ->
		derivewatchLayers["Categories"].subLayers[6].visible = false
		derivewatchLayers["Categories"].subLayers[5].visible = true
		derivewatchLayers["curious_on"].ignoreEvents = false
		derivewatchLayers["curious_off"].ignoreEvents = true
	
# Settings Screen Helper Functions
settingsInit = () ->
	
	derivewatchLayers["settings_back"].off Events.Click
	
	derivewatchLayers["settings_back"].ignoreEvents = false
	derivewatchLayers["settings_icon"].ignoreEvents = true
	derivewatchLayers["cafe_selected"].ignoreEvents = true
	derivewatchLayers["sightglass_unselected"].ignoreEvents = true
	derivewatchLayers["curious_on"].ignoreEvents = true
	derivewatchLayers["curious_off"].ignoreEvents = true
	derivewatchLayers["set_destination"].ignoreEvents = true
	derivewatchLayers["kim_selected"].ignoreEvents = true
	derivewatchLayers["kim_unselected"].ignoreEvents = true
	derivewatchLayers["invite_friends"].ignoreEvents = true	
	# Back Button Functionality
	settingsBackButtonAnimation = new Animation({
	    layer: derivewatchLayers["Settings"],
	    properties: {opacity:0},
	    time: 0.2
	})
	
	derivewatchLayers["settings_back"].on Events.Click, ->
		settingsBackButtonAnimation.start()
		
	settingsBackButtonAnimation.on Events.AnimationEnd, -> 
		derivewatchLayers["Categories"].opacity = 1
		derivewatchLayers["Categories"].visible = true
		categoriesInit()

# List of Cafes Screen Helper Functions
listOfCafesInit = () ->
	
	derivewatchLayers["settings_back"].off Events.Click
	
	derivewatchLayers["settings_back"].ignoreEvents = false
	derivewatchLayers["settings_icon"].ignoreEvents = true
	derivewatchLayers["cafe_selected"].ignoreEvents = true
	derivewatchLayers["sightglass_unselected"].ignoreEvents = false
	derivewatchLayers["curious_on"].ignoreEvents = true
	derivewatchLayers["curious_off"].ignoreEvents = true
	derivewatchLayers["set_destination"].ignoreEvents = true
	derivewatchLayers["kim_selected"].ignoreEvents = true
	derivewatchLayers["kim_unselected"].ignoreEvents = true
	derivewatchLayers["invite_friends"].ignoreEvents = true
	
	derivewatchLayers["List_of_Cafes"].subLayers[0].visible = false
	derivewatchLayers["List_of_Cafes"].subLayers[1].visible = true
	
	# Back Button Functionality
	backButtonAnimation = new Animation({
	    layer: derivewatchLayers["List_of_Cafes"],
	    properties: {opacity:0},
	    time: 0.2
	})
	
	derivewatchLayers["settings_back"].on Events.Click, ->
		backButtonAnimation.start()
		
	backButtonAnimation.on Events.AnimationEnd, -> 
		derivewatchLayers["Categories"].opacity = 1
		derivewatchLayers["Categories"].visible = true
		categoriesInit()
	
	# Sightglass Selected Functionality
	sightglassSelectedAnimation = new Animation({
	    layer: derivewatchLayers["List_of_Cafes"],
	    properties: {opacity:0},
	    time: 0.2
	})
	
	derivewatchLayers["sightglass_unselected"].on Events.Click, -> 
		derivewatchLayers["List_of_Cafes"].subLayers[0].visible = true
		derivewatchLayers["List_of_Cafes"].subLayers[1].visible = false
		sightglassSelectedAnimation.start()
	
	sightglassSelectedAnimation.on Events.AnimationEnd, -> 
		derivewatchLayers["Sightglass_Coffee"].opacity = 1
		derivewatchLayers["Sightglass_Coffee"].visible = true
		sightglassCoffeeInit()

# Sightglass Coffee Screen Helper Functions
sightglassCoffeeInit = () ->
	
	derivewatchLayers["settings_back"].off Events.Click
	
	derivewatchLayers["settings_back"].ignoreEvents = false
	derivewatchLayers["settings_icon"].ignoreEvents = true
	derivewatchLayers["cafe_selected"].ignoreEvents = true
	derivewatchLayers["sightglass_unselected"].ignoreEvents = true
	derivewatchLayers["curious_on"].ignoreEvents = true
	derivewatchLayers["curious_off"].ignoreEvents = true
	derivewatchLayers["set_destination"].ignoreEvents = false
	derivewatchLayers["kim_selected"].ignoreEvents = true
	derivewatchLayers["kim_unselected"].ignoreEvents = true
	derivewatchLayers["invite_friends"].ignoreEvents = true
	
	# Back Button Functionality
	backButtonAnimation = new Animation({
	    layer: derivewatchLayers["Sightglass_Coffee"],
	    properties: {opacity:0},
	    time: 0.2
	})
	
	derivewatchLayers["settings_back"].on Events.Click, ->
		backButtonAnimation.start()
		
	backButtonAnimation.on Events.AnimationEnd, -> 
		derivewatchLayers["List_of_Cafes"].opacity = 1
		derivewatchLayers["List_of_Cafes"].visible = true
		listOfCafesInit()
	
	# Sightglass Selected Functionality
	setDestinationButtonAnimation = new Animation({
	    layer: derivewatchLayers["Sightglass_Coffee"],
	    properties: {opacity:0},
	    time: 0.2
	})
	
	derivewatchLayers["set_destination"].on Events.Click, ->
		setDestinationButtonAnimation.start()
		
	setDestinationButtonAnimation.on Events.AnimationEnd, -> 
		derivewatchLayers["INVITE_FRIENDS"].opacity = 1
		derivewatchLayers["INVITE_FRIENDS"].visible = true
		inviteFriendsInit()

# Invite Friends Screen Helper Functions
inviteFriendsInit = () ->
	
	derivewatchLayers["settings_back"].off Events.Click
	
	derivewatchLayers["settings_back"].ignoreEvents = false
	derivewatchLayers["settings_icon"].ignoreEvents = true
	derivewatchLayers["cafe_selected"].ignoreEvents = true
	derivewatchLayers["sightglass_unselected"].ignoreEvents = true
	derivewatchLayers["curious_on"].ignoreEvents = true
	derivewatchLayers["curious_off"].ignoreEvents = true
	derivewatchLayers["set_destination"].ignoreEvents = true
	derivewatchLayers["invite_friends"].ignoreEvents = false
	
	derivewatchLayers["kim_selected"].ignoreEvents = true
	derivewatchLayers["kim_unselected"].ignoreEvents = false

	derivewatchLayers["INVITE_FRIENDS"].subLayers[0].visible = false
	derivewatchLayers["INVITE_FRIENDS"].subLayers[1].visible = true
	
	# Back Button Functionality
	backButtonAnimation = new Animation({
	    layer: derivewatchLayers["INVITE_FRIENDS"],
	    properties: {opacity:0},
	    time: 0.2
	})
	
	derivewatchLayers["settings_back"].on Events.Click, ->
		backButtonAnimation.start()
		
	backButtonAnimation.on Events.AnimationEnd, -> 
		derivewatchLayers["Sightglass_Coffee"].opacity = 1
		derivewatchLayers["Sightglass_Coffee"].visible = true
		sightglassCoffeeInit()
	
	# Select Friends Functionality
	derivewatchLayers["kim_selected"].on Events.Click, ->
		derivewatchLayers["INVITE_FRIENDS"].subLayers[0].visible = false
		derivewatchLayers["INVITE_FRIENDS"].subLayers[1].visible = true
		derivewatchLayers["kim_selected"].ignoreEvents = true
		derivewatchLayers["kim_unselected"].ignoreEvents = false
	
	derivewatchLayers["kim_unselected"].on Events.Click, ->
		derivewatchLayers["INVITE_FRIENDS"].subLayers[0].visible = true
		derivewatchLayers["INVITE_FRIENDS"].subLayers[1].visible = false
		derivewatchLayers["kim_selected"].ignoreEvents = false
		derivewatchLayers["kim_unselected"].ignoreEvents = true
		
	# Invite Friends Functionality
	inviteFriendsButtonAnimation = new Animation({
	    layer: derivewatchLayers["INVITE_FRIENDS"],
	    properties: {opacity:0},
	    time: 0.2
	})
	
	derivewatchLayers["invite_friends"].on Events.Click, ->
		inviteFriendsButtonAnimation.start()
		
	inviteFriendsButtonAnimation.on Events.AnimationEnd, -> 
		derivewatchLayers["Select_Time"].opacity = 1
		derivewatchLayers["Select_Time"].visible = true
		selectTimeInit()

# Select Time Screen Helper Functions
selectTimeInit = () ->	
	derivewatchLayers["settings_back"].off Events.Click
	
	derivewatchLayers["settings_back"].ignoreEvents = false
	derivewatchLayers["settings_icon"].ignoreEvents = true
	derivewatchLayers["cafe_selected"].ignoreEvents = true
	derivewatchLayers["sightglass_unselected"].ignoreEvents = true
	derivewatchLayers["curious_on"].ignoreEvents = true
	derivewatchLayers["curious_off"].ignoreEvents = true
	derivewatchLayers["set_destination"].ignoreEvents = true
	derivewatchLayers["invite_friends"].ignoreEvents = false
	
	derivewatchLayers["kim_selected"].ignoreEvents = true
	derivewatchLayers["kim_unselected"].ignoreEvents = true
	
	# Back Button Functionality
	backButtonAnimation = new Animation({
	    layer: derivewatchLayers["Select_Time"],
	    properties: {opacity:0},
	    time: 0.2
	})
	
	derivewatchLayers["settings_back"].on Events.Click, ->
		backButtonAnimation.start()
		
	backButtonAnimation.on Events.AnimationEnd, -> 
		derivewatchLayers["INVITE_FRIENDS"].opacity = 1
		derivewatchLayers["INVITE_FRIENDS"].visible = true
		inviteFriendsInit()
	
	# Time Selected Functionality
	selectTimeAnimation = new Animation({
	    layer: derivewatchLayers["Select_Time"],
	    properties: {opacity:0},
	    time: 0.2
	})
	
	derivewatchLayers["Select_Time"].on Events.Click, ->
		selectTimeAnimation.start()
		
	selectTimeAnimation.on Events.AnimationEnd, -> 
		derivewatchLayers["Start_Screen"].opacity = 1
		derivewatchLayers["Start_Screen"].visible = true
	
# Splash Screen to Categories Transition
derivewatchLayers["Splash_Screen"].animate({
    properties: {opacity:0},
    delay: 1.5
})

derivewatchLayers["Splash_Screen"].on Events.AnimationEnd, -> 
    derivewatchLayers["Categories"].opacity = 1
    derivewatchLayers["Categories"].visible = true
    categoriesInit()


		

